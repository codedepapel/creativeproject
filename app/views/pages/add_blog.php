<?php require  APPROOT . '/views/inc/header.php'?>
<div class="container">
    <div class="row">
      <div class="col-md-12">
          <form class="mt-5 mb-5 mx-auto" id="adminFormPost"  action="<?php echo URLROOT; ?>/pages/blog" method="post">
            <div class="form-group">
              <label for="blogPostTitle">Τίτλος</label>
              <input type="text" name="blog_title" class="form-control" id="blogPostTitle">
            </div>
            <div class="form-group">
              <label for="blogPostBody">Κυρίως Μήνυμα</label>
              <textarea class="form-control" name="blog_body" id="blogPostBody" rows="14"></textarea>
            </div>
            <button type="button" class="btn btn-outline-success btn-lg">Αποστολή</button>
        </form>
      </div>
    </div> <!--row-->
</div> <!--container-->


<?php require  APPROOT . '/views/inc/footer.php'?>