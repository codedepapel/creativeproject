<?php require  APPROOT . '/views/inc/header.php'?>


<div class="row">
    <div class="col-md-12" id="imgblog">
        <div class="col-8 offset-2" id="blogpage">
            <p id="blogpagetitle" >Blog</p>
            <p id="blogdescription" > Μάθε τα πάντα για την γυμναστική και τη σωστή διατροφή!</p>
        </div>
    </div>
    <?php 
    if(isset($_SESSION['isAdmin'])){ ?>
        <div class="col-8 offset-2 mb-5">
            <a href="<?php echo URLROOT; ?>/pages/add_blog"  class="btn btn-info btn-lg btn-block">Ανέβασε το δικό σου άρθρο</a>
        </div>
    <?php } ?>
</div>
<div class="container">
    <?php foreach ($data as $article) : ?>
        <div class="row">
            <div class="col-md-6 anime">
               <a href="<?php echo URLROOT.'/pages/article/'.$article->post_id;?>"><img src= " <?php echo URLROOT.'/images/blog/'.$article->post_image;?>" style="margin-bottom:15px;" width="100%" alt="stars"/></a>
            </div>
            <div class="col-md-6 blogbox anime-left">
            <a href="<?php echo URLROOT.'/pages/article/'.$article->post_id;?>"> <h4><?php echo $article->post_title; ?></h4></a>
                <br>
                <p class="maxLength"><?php echo $article->post_description; ?></p>
                </div>
        </div>
    <?php endforeach; ?>
</div>
 
<!---Animation on Scroll--------->

<script type="text/javascript">

debounce = function(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};


(function(){
	var $target = $('.anime'),
			animationClass = 'anime-start',
			offset = $(window).height() * 3/4;

	function animeScroll() {
		var documentTop = $(document).scrollTop();

		$target.each(function(){
			var itemTop = $(this).offset().top;
			if (documentTop > itemTop - offset) {
				$(this).addClass(animationClass);
			} 
		});
	}

	animeScroll();

	$(document).scroll(debounce(function(){
		animeScroll();
	}, 100));
})();



(function(){
	var $target = $('.anime-left'),
			animationClass = 'anime-end',
			offset = $(window).height() * 3/4;

	function animeScroll() {
		var documentTop = $(document).scrollTop();

		$target.each(function(){
			var itemTop = $(this).offset().top;
			if (documentTop > itemTop - offset) {
				$(this).addClass(animationClass);
			}
		});
	}

	animeScroll();

	$(document).scroll(debounce(function(){
		animeScroll();
	}, 100));
})();


</script>

<?php require  APPROOT . '/views/inc/footer.php'?>