Authors: Theodora Galata, George Georgakas, Panos Paoulinis

Online Website: https://www.georgegeorgakas.com/gymaround
Localhost: localhost/gymaround

This is the documentation for our website.

Bit Bucket repository: git clone https://george_geo@bitbucket.org/codedepapel/creativeproject.git

Install Composer for this project

1. https://getcomposer.org/
2. cd /gymaround
3. composer update
4. You are good to go! :)

Our database is already set up on config.php.
If you need to change anything for online deploy you can do it from there.
